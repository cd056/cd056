//read data from the keyboard, write it to a file called INPUT, again read the same data from the INPUT file, and display it on the screen/console
#include<stdio.h>
int main()
{
	FILE *fp;
	char c;
	printf("Input data : ");
	fp=fopen("input.dat","w");
	while((c=getchar())!=EOF)
		fputc(c,fp);
	fclose(fp);
	printf("Data output : ");
	fp=fopen("input.dat","r");
	while((c=fgetc(fp))!=EOF)
		printf("%c",c);
	fclose(fp);
	return 0;
}