//to find the possible roots of quadratic equation using switch case statement
#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    int k;
    float d,r1,r2;
    printf("Enter the three coefficients of the quadratic equation : ");
    scanf("%d,%d,%d",&a,&b,&c);
    d=(b*b)-(4*a*c);
    if(d>0)
    { k=1; }
    else if(d==0)
    { k=2; }
    else
    { k=3; }
    switch(k)
    {
        case 1: r1=(-b+sqrt(d))/(2*a);
                r2=(-b-sqrt(d))/(2*a);
                printf("The two roots of the quad eq are %f and %f. \n",r1,r2);
                break;
        case 2: r1=(-b)/(2*a);
                printf("The unique root of the quad eq is %f. \n",r1);
                break;
        case 3: printf("The roots for the quad eq is imaginary. \n");
    }
    return 0;
}