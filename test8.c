//program to check the given string is palindrome or not
#include<stdio.h>
int main()
{
	char s[1000];
	int i,flag,len,c;
	flag=0;
	printf("Enter any string to be checked for palindrome : ");
	gets(s);

	c=0;
	while(s[c]!='\0')
		c++;
	len=c;

	for(i=0;i<len;i++)
	{
		if(s[i]!=s[len-i-1])
		{
			flag=1; 
			break;
		}
	}

	if(flag==0)
		printf("\n%s is a Palindrome.\n",s);
	else if(flag==1)
		printf("\n%s is not a palindrome.\n",s);

	return 0;
}