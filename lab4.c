//to enter a number and calculate the sum of its digits.
#include<stdio.h>
int main()
{
	int num, sum=0;
	printf("Enter the number whose sum of digits is to be found : ");
	scanf("%d",&num);
	while(num!=0)
	{
		int r=num%10;
		sum=sum+r;
		num=num/10;
	}
	printf("The sum of the digits is : %d \n",sum);
	return 0;
}