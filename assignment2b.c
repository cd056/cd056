#include<stdio.h>
float input()
{
    float a;
    printf("Enter the value:\n");
    scanf("%f",&a);
    return a;
}
float areaoftri(float b,float h)
{
    float area_tri=0.5*b*h;
    return area_tri;
}
void display(float area)
{
    printf("The area of the triangle is %f!",area);
}
int main()
{
    float b,h,area;
    printf("Enter the base and height of the triangle respectively.");
    b=input();
    h=input();
    area=areaoftri(b,h);
    display(area); 
    return 0;
}