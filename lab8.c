//read and print employee details using structures
#include<stdio.h>
typedef struct FullName
{
	char first_name[50];
	char second_name[50];
} name;
typedef struct Employee
{
	int eno;
	name n1;
	float salary;
	char DOB[80];
	char company_name[80];
} emp;
int main()
{
	emp e1;
	printf("Enter the employee number : ");
	scanf("%d",&e1.eno);
	printf("Enter the employee First name : ");
	scanf("%s",e1.n1.first_name);
	printf("Enter the employee Second name : ");
	scanf("%s",e1.n1.second_name);
	printf("Enter the salary of the employee : ");
	scanf("%f",&e1.salary);
	printf("Enter the Date Of Birth of employee : ");
	scanf("%s",e1.DOB);
	printf("Enter the Company name of employee : ");
	scanf("%s",e1.company_name);

	printf("\n\n-----EMPLOYEE DETIALS-----\n");
	printf("Employee number : %d",e1.eno);
	printf("\nEmployee name : %s %s",e1.n1.first_name,e1.n1.second_name);
	printf("\nEmployee salary : %f",e1.salary);
	printf("\nEmployee Date of Birth : %s",e1.DOB);
	printf("\nEmployee company name : %s",e1.company_name);
	return 0;
}