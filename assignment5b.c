//store missed element 10
#include<stdio.h>
int main()
{
    int odd[10]={1,3,5,7,9,11,13,15},n=8,num_del;
    printf("The elements stored in the array are :\n");
    for(int i=0;i<n;i++)
        printf("%d ",odd[i]);
    //elements of array are printed
    num_del=5;
    for(int i=0;i<n;i++)
    {
        if(odd[i]==num_del)
        {
            for(int j=i;j<n-1;j++)
            {    
                odd[j]=odd[j+1];
            }
            odd[n]=0;
            n--;
            break; 
        }
        else
        {
            continue;
        }
    }
    //element 5 has been deleted
    printf("\nThe elements stored in the array now are: \n");
    for(int i=0;i<n;i++)
        printf("%d ",odd[i]);
    return 0;
}