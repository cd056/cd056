//since the given array is unsorted, LINEAR searching is more efficient
#include<stdio.h>
void linear_search(int array[10],int search,int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        if (array[i] == search) /* If required element is found */
        {
            printf("The element %d is present at location %d.\n", search, i);
            break;
        }
    }
    if (i == n)
        printf("%d isn't present in the array.\n", search);
}
int main()
{
    int a[10]={23,27,5,7,33,17,7,19,21,57},n=10;
    int num=21;
    printf("The number to be searched is %d\n",num);
    linear_search(a,num,n);
    return 0;
}