//company decides to give bonus to all its employees on Diwali. 
#include<stdio.h>
int main()
{
    float bonus,salary;
    char sex;
    printf("Enter the salary(in Rupees) and the sex(F for female/M for male) of the employee: \n");
    scanf("%f,%c",&salary,&sex);
    if((sex=='F')||(sex=='f'))               //check if employee is female or male and give bonus
    { 
        if(salary<10000)
        {
        bonus=0.12*salary;
        salary=salary+bonus;
        }
        else
        {
        bonus=0.10*salary;
        salary=salary+bonus;
        }
    }
    else if((sex=='M')||(sex=='m'))
    {
        if(salary<10000)
        {
        bonus=0.07*salary;
        salary=salary+bonus;
        }
        else
        {
        bonus=0.05*salary;
        salary=salary+bonus;
        }
    }
    else
    {
        printf("Enter either F or M only!");
    }
    
    printf("The bonus of the employee will be : %f \nThe salary of the employee is : %f.",bonus,salary);
    return 0;
}



