#include<stdio.h>
#define pi 3.14
float input()
{
    float a;
    printf("Enter the radius of the circle:\n");
    scanf("%f",&a);
    return a;
}
float areaofcircle(float a)
{
    float area_circle=pi*a*a;
    return area_circle;
}
void display(float area)
{
    printf("The area of the circle is %f!",area);
}
int main()
{
    float r,area;
    r=input();
    area=areaofcircle(r);
    display(area);
    return 0;
}