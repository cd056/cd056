//Illustrate pointers in swapping two numbers
#include<stdio.h>
void swap(int *var1,int *var2)
{
	int temp;
	temp=*var1;
	*var1=*var2;
	*var2=temp;
}
int main()
{
	int a=10, b=20;
	printf("THE VALUE OF a AND b BEFORE SWAPPING IS : \n");
	printf("a= %d\n",a);
	printf("b= %d\n",b);
	swap(&a,&b);
	printf("THE VALUE OF a AND b AFTER SWAPPING IS : \n");
	printf("a= %d\n",a);
	printf("b= %d\n",b);
	return 0;
}