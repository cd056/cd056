//convert a decimal number to a binary number.
#include<math.h>
#include<stdio.h>
int main()
{
	int binary=0,reminder=0,decimal,i=0;
	printf("Enter the decimal number : ");
	scanf("%d",&decimal);
	int num=decimal;
	while(decimal!=0)
	{
		reminder=decimal%2;
		binary=binary+(reminder*pow(10,i));
		decimal=decimal/2;
		i++;
	}
	printf("The binary number of the decimal %d is %d.\n",num,binary);
	return 0;
}