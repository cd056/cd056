//since the given array is increasingly sorted array, BINARY searching is more efficient
#include<stdio.h>
int binary_search(int array[10],int search_element,int n)
{
int beg=0,mid,end=n-1;
while(beg <= end)
{
mid=(beg+end)/2;
if(array[mid]<search_element)
beg=mid+1;
else if(array[mid] == search_element)
return mid;
else
end=mid-1;
}
return n+1;
}
int main()
{
int a[10]={2,3,5,7,11,13},n=6;
int num=11;
printf("The number to be searched is %d\n",num);
int k=binary_search(a,num,n);
if(k!=n+1)
printf("The element %d exists in the array at position %d.\n",num,k+1);
else
printf("The element doesn't exist in the array.\n");
return 0;
}