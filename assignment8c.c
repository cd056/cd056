//check whether the given number is palindrome or not
#include<stdio.h>
int main()
{
	int num,original,reverse=0,reminder;
	printf("Enter a number : ");
	scanf("%d",&num);
	original=num;
	while(num!=0)
	{
		reminder=num%10;
		reverse=(reverse*10)+reminder;
		num=num/10;
	}
	if(reverse==original)
		printf("%d is a palindrome number.\n",original);
	else
		printf("%d is not a palindrome number.\n",original);
	return 0;
}