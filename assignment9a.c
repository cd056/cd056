//program to print Sum and Difference of a 2D array
#include<stdio.h>
int main()
{
	int m,n,i,j,first[10][10],second[10][10],sum[10][10],diff[10][10];
	printf("Enter the number of rows and columns of the matrix : ");
	scanf("%d,%d",&m,&n);

	printf("Enter the elements into the first matrix : \n");
	for(i=0;i<m;i++)
		for(j=0;j<n;j++)
			scanf("%d",&first[i][j]);

	printf("Enter the elements into the second matrix : \n");
	for(i=0;i<m;i++)
		for(j=0;j<n;j++)
			scanf("%d",&second[i][j]);

	printf("THE SUM OF THE TWO MATRIX ARE : \n");
	for(i=0;i<m;i++)
	{
		printf("\n    ");
		for(j=0;j<n;j++)
		{
			sum[i][j]=first[i][j]+second[i][j];
			printf("%d  ",sum[i][j]);
		}
	}

	printf("\nTHE DIFFERENCE OF THE TWO MATRIX ARE : \n");
	for(i=0;i<m;i++)
	{
		printf("\n    ");
		for(j=0;j<n;j++)
		{
			diff[i][j]=first[i][j]-second[i][j];
			printf("%d  ",diff[i][j]);
		}
	}

	return 0;
}