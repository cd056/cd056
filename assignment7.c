/* to print the pattern as shown below:
    1
  2 1
3 2 1                                    */

//trail 1
#include<stdio.h>
int main()
{   
    int n;
	printf("Enter the value of n :");
	scanf("%d",&n);
	for(int i=n;i>0;i--)                         //loop for rows with i=3,2,1 for n=3
	{
		for(int j=n;j>0;j--)                     //loop for coloumn with j=3,2,1
		{
			if(j>(n+1-i))                        
				printf("  ");
			else
				printf("%d ",j);
		}
		printf("\n");
	}
	return 0;
}

//trail 2
#include<stdio.h>
int main()
{
	int n;
	printf("Enter the value of n :");            
	scanf("%d",&n); 
	for(int j=1;j<=n;j++)                       //loop for rows with j=1,2,3 with n=3
	{
		int k=n-1;                              //k=2
	/*while(2>=j) i.e prints 2 " " when (j=1), prints 1 " " when (j=2), no " " when (j=3) */
		while(k>=j)                             //to print req number of spaces
		{
			printf("  ");
			k--;
		}
		for(int i=j;i>0;i--)                    //to print pattern in ascending order
			printf("%d ",i);
		printf("\n");
	}
	return 0;
}