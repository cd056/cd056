//find average of n numbers with array
//accepts input elements into the array and then finds average 
#include<stdio.h>
int main()
{
	int a[20],sum=0;
	float avg=0.0,n;
	printf("Enter the number of elements in the array: ");
	scanf("%f",&n);
    printf("Enter the elements into the array : \n");
	for(int i=0;i<n;i++)
	{   
		scanf("%d",&a[i]);
	    sum=sum+a[i];
	}
	avg=sum/n;
	printf("The average is %f. \n",avg);
	return 0;
}