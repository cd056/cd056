/* create student structure, read two student details
and print the student details who has scored the highest. */
#include<stdio.h>
struct name 
{
	char firstname[50];
	char secondname[50];
};
struct student
{
	int rno,total;
	struct name n1;
	float fees;
	char section[5];
	char department[5];
};
int main()
{
	struct student s[2];
	int large=0;
	for(int i=0;i<2;i++)
	{
		printf("ENTER STUDENT DETAILS:\n");
		printf("Roll number : ");
		scanf("%d",&s[i].rno);
		printf("Full Name : ");
		scanf("%s %s",s[i].n1.firstname,s[i].n1.secondname);
		printf("Fees : ");
		scanf("%f",&s[i].fees);
		printf("Section : ");
		scanf("%s",s[i].section);
		printf("Department : ");
		scanf("%s",s[i].department);
		printf("Total marks obtained out of 500 : ");
		scanf("%d",&s[i].total);
	}
	if(s[0].total>=s[1].total)
		large=s[0].total;
	else
		large=s[1].total;
	for(int i=0;i<2;i++)
	{
		if(s[i].total==large)
		{
			printf("\nDETAILS OF STUDENT WITH HIGHEST MARKS :\n");
			printf("Roll number : %d",s[i].rno);
			printf("\nName : %s %s",s[i].n1.firstname,s[i].n1.secondname);
			printf("\nFees : %f",s[i].fees);
			printf("\nSection : %s",s[i].section);
			printf("\nDepartment : %s",s[i].department);
			printf("\nTotal marks obtained : %d",s[i].total);
		} 
	}
	return 0;
}