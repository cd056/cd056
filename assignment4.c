#include<stdio.h>
int main()
{
    int a,b;
    printf("Enter two numbers to be swapped : \n");
    scanf("%d%d",&a,&b);
    printf("The initial values are :\na=%d \nb=%d \n",a,b);
    a=a+b;
    b=a-b;
    a=a-b;
    printf("Values after swapping are :\na=%d \nb=%d \n",a,b);
    return 0;
}