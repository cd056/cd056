//reading a two-dimensional array marks which stores marks of 5 students in 3 subjects and display the highest marks in each subject.

#include<stdio.h>
int main()
{
	int marks[5][3],i,j,max_marks;     

	for(i=0;i<5;i++)
	{
		printf("Enter the marks obtained by the %d'th student : \n",i+1);
		for(j=0;j<3;j++)
		{
			printf("Enter marks in subject %d : ",j+1);
			scanf("%d",&marks[i][j]);
		}
	}

	for(j=0;j<3;i++)
	{
		max_marks=0;
		for(i=0;i<5;i++)
		{
			if(marks[i][j]>max_marks)
				max_marks=marks[i][j];
		}
		printf("The highest marks in %d'th subject is : %d.\n",j+1,max_marks);
	}

	return 0;
}