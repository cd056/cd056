#include<stdio.h>
float input()
{
    float a;
    printf("Enter the value: ");
    scanf("%f",&a);
    return a;
}
float hr2min(float h,float m)
{
    float ans=(h*60)+m;
    return ans;
}
void display(float ans)
{
    printf("The time in minutes is %f!",ans);
}
int main()
{
    float hr,min,ans;
    printf("Enter the values of time in hour and minutes respectively.\n");
    hr=input();
    min=input();
    ans=hr2min(hr,min);
    display(ans); 
    return 0;
}