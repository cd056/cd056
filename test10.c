// program to perform arithmetic operations (addition, subtraction, multiplication, division and remainder) on a two integers using pointers.
#include<stdio.h>
void add(int *a,int *b, int *ans)
{ *ans=*a+*b; }
void sub(int *a,int *b, int *ans)
{ *ans=*a-*b; }
void mul(int *a,int *b, int *ans)
{ *ans=(*a)*(*b); }
void divv(int *a,int *b, int *ans)
{ *ans=(*a)/(*b); }
void remm(int *a,int *b, int *ans)
{ *ans=(*a)%(*b); }
int main()
{
	int num1,num2,sum,diff,prod,quot,rem;
	printf("Enter two numbers :\n");
	scanf("%d,%d",&num1,&num2);
	add(&num1,&num2,&sum);
	printf("Sum of the two numbers is = %d \n",sum);
	sub(&num1,&num2,&diff);
	printf("Difference of the two numbers is = %d \n",diff);
	mul(&num1,&num2,&prod);
	printf("Product of the two numbers is = %d \n",prod);
	divv(&num1,&num2,&quot);
	printf("Quotient of the two numbers is = %d \n",quot);
	remm(&num1,&num2,&rem);
	printf("Remainder of the two numbers is = %d \n",rem);
	return 0;
}