//interchange the largest and smallest number in the given array
#include<stdio.h>
int main()
{
	int a[20],n,small,large,small_pos,large_pos,temp;
	printf("Enter the size of the array :");
	scanf("%d",&n);
	printf("\nEnter the elements into the array : \n");
	for(int i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	small=large=a[0];
	small_pos=large_pos=0;
	for(int i=0;i<n;i++)
	{
		if(a[i]<small)
		{
			small=a[i];
			small_pos=i;
		}
		if(a[i]>large)
		{
			large=a[i];
			large_pos=i;
		}
	}
	temp=a[large_pos];
	a[large_pos]=a[small_pos];
	a[small_pos]=temp;
	printf("The array after swapping is : \n");
	for(int i=0;i<n;i++)
	    printf("%d ",a[i]);
	return 0;
}
