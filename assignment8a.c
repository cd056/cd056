//to classify a given number as prime or composite
#include<stdio.h>
int main()
{
    int flag=0,num;
    printf("Enter the number : ");
    scanf("%d",&num);
    
    for(int i=2;i<=num/2;i++)
    {
        if(num%i==0)
        {
            flag=1;
            break;
        }
    }
    
    if(flag==0)
        printf("%d is a prime number.\n",num);
    else
        printf("%d is a composite number.\n",num);
    return 0;
}