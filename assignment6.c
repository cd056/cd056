//to find the second largest number in the array
#include<stdio.h>
int main()
{
	int a[20],n;
	int max1,max2;
	printf("Enter the size of the array : ");
	scanf("%d",&n);
	printf("Enter the elements into the array : ");
	for(int i=0;i<n;i++)
		scanf("%d",&a[i]);
	max1=max2=0;                     //max1 is largest & max2 is the second largest
	for(int i=0;i<n;i++)
	{
		if(a[i]>max1)                //if the elements a[i] is greater than the largest number, it means that stored value of max1 is not the largest
		{
			max2=max1;
			max1=a[i];
		}
		else if((a[i]>max2)&&(a[i]<max1)) /*if the element a[i] is greater than the second largest but less than the largest, it means that the number stored in max2 is not the sec largest */
		{
			max2=a[i];
		}
	}
	printf("The second largest element in the array is %d.",max2);
	return 0;
}   