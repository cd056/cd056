//read and print the 2 dimensional array
#include<stdio.h>
int main()
{
	int abc[3][3],i,j;
    
	printf("Enter the values into the (3x3) 2 dimentional array - \n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("Enter the value for abc[%d][%d] : ",i,j);
			scanf("%d",&abc[i][j]);
		}
	}

	printf("\nThe 2 dimensional (3x3) array elements are : \n");
	for(i=0;i<3;i++)
	{
		printf("\n    ");
		for(j=0;j<3;j++)
		{
			printf("%d  ",abc[i][j]);
		}
	}

	return 0;
}