#include<stdio.h>
#include<math.h>
void display_root(int a,int b,int d)
{   
    float r1,r2;
	float real,img;
    if(d>0)
	{
		printf("The roots of the quadratic equation are real and distinct.\n");
		r1=(-b+sqrt(d))/(2*a);
		r2=(-b-sqrt(d))/(2*a);
		printf("The two roots are root1=%f and root2=%f.\n",r1,r2);
	}
	else if(d==0)
	{
		r1=-b/(2*a);
		printf("There is only one root of this quadratic equation and it is, r1 =%f.\n",r1);
	}
	else
	{ 
		printf("The roots of this quadratic equation is distinct and imaginary.\n");
		real=-b/(2*a);
		img=sqrt(-d)/(2*a);
		printf("The roots are %2.0f+i%4.3f and %2.0f-i%4.3f.",real,img,real,img);
	}
}
int main()
{
	int a,b,c;
	float d;
	printf("The the values a,b & c for the quadratic equation :\n");
	scanf("%d,%d,%d",&a,&b,&c);
	d=(b*b)-(4*a*c);
	display_root(a,b,d);
	return 0;
}